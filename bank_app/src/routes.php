<?php

require_once __DIR__ . '/helpers/router.php';

// global httpOnly setting
ini_set('session.cookie_httponly', 1);

// auth
get('/', 'views/login.php');
post('/', 'views/login.php');
get('/sign-up', 'views/sign-up.php');
post('/sign-up', 'views/sign-up.php');
get('/logout', 'views/logout.php');
get('/upload', 'views/upload.php');
post('/upload', 'views/upload.php');

// dashboard
get('/dashboard', 'views/dashboard.php');
get('/employee-dashboard', 'views/employee-dashboard.php');
get('/all-uploads', 'views/all-uploads.php');

// files
get('/uploads/$file', 'views/file.php');

// apis
post('/api/create-chat', 'api/api-create-chat.php');
post('/api/get-chat-id', 'api/api-get-chat-id.php');
post('/api/get-latest-messages', 'api/api-get-latest-messages.php');
get('/api/get-session-id', 'api/api-get-session-id.php');
post('/api/send-message', 'api/api-send-message.php');

// 404
any('/404', 'templates/404.php');