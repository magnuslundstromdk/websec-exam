<?php
require_once __DIR__ . '/../helpers/out.php';
$title = "Employee Dashboard";
require_once __DIR__ . '/components/head.php';
?>
<header>
    <a href="/logout" style="margin-right:30px;">Logout</a>
    <a href="/employee-dashboard" style="margin-right: 30px;">Dashboard</a>
    <a href="/all-uploads">Uploads</a>
</header>
<h1>All uploads</h1>
<section>
    <p style="font-weight: 700;">See all uploads!</p>

    <?php
    foreach ($files as $file) {
    ?>
    <a href="/uploads/<?= out($file) ?>" style="display: block;" target="_blank"><?= out($file) ?></a>
    <?php
    }
    ?>
</section>