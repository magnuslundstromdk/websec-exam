<?php
$title = "Employee Dashboard";
require_once __DIR__ . '/components/head.php';
?>
<header>
    <a href="/logout" style="margin-right: 30px;">Logout</a>
    <a href="/employee-dashboard" style="margin-right: 30px;">Dashboard</a>
    <a href="/all-uploads">Uploads</a>
</header>
<h1>Employee Dashboard</h1>
<section>
    <p>Welcome back to work <?= $email ?></p>
</section>
<?php require_once __DIR__ . '/components/customer-list.php' ?>
<?php require_once __DIR__ . '/components/chat.php' ?>
<?php require_once __DIR__ . '/components/footer.php' ?>