<?php require_once __DIR__ . '/../../helpers/csrf.php'; ?>
<section id="chat">
    <div id="chat-container"></div>
    <form>
        <input type="text" name="message_body" placeholder="Type your message here...">
        <input type="hidden" name="chat_id">
        <?php set_csrf() ?>
        <button>Send</button>
    </form>
</section>