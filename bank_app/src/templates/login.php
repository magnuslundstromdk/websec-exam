<?php

$title = "Login";
require_once __DIR__ . '/components/head.php';
require_once __DIR__ . '/../helpers/csrf.php'
?>

<h1>Login</h1>
<section>
    <form action="" method="POST">
        <?php set_csrf(); ?>
        <input type="email" name="email" placeholder="Email">
        <input type="password" name="password" placeholder="Password">
        <button>Submit</button>
        <p class="error"><?= $general_error_message ?></p>
    </form>
    <p>
        <a href="/sign-up">Sign up!</a>
    </p>
</section>

<?php require_once __DIR__ . '/components/footer.php' ?>