<?php

$title = "Upload document";
require_once __DIR__ . '/components/head.php';
?>
<header>
    <a href="/logout" style="margin-right: 30px;">Logout</a>
    <a href="/dashboard" style="margin-right: 30px;">Dashboard</a>
    <a href="/upload">Upload</a>
</header>
<h1>Upload document </h1>
<p>Please upload your documents to verify your identity. The document must be in pdf format and maximum of 5Mb.</p>
<form action="" method="POST" enctype="multipart/form-data">
    <h2>Select file</h2>
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload file" name="submit">
</form>
<p class="error"><?= $errorMessage ?></p>

<?php require_once __DIR__ . '/components/footer.php'; ?>