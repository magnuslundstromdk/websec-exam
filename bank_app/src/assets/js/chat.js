const displayChatButton = document.querySelector("#display-chat");
const chat = document.querySelector("#chat");
const chatContainer = document.querySelector("#chat-container");
const chatForm = chat.childNodes[3];
const customerChats = document.querySelectorAll(".customer-list-item");

const scrollToBottom = (element) => {
	element.scrollTop = element.scrollHeight;
};

const htmlEncode = (str) => {
	return String(str).replace(/[^\w. ]/gi, function (c) {
		return "&#" + c.charCodeAt(0) + ";";
	});
};

let latestMessageId = 0;
const getLatestMessages = async (chatId) => {
	let form = new FormData();
	form.append("latest_message_id", latestMessageId);
	form.append("chat_id", chatId);
	const responseMessages = await fetch("/api/get-latest-messages", {
		method: "POST",
		body: form,
		headers: { "Cache-Control": "no-cache" },
	});

	if (!responseMessages.ok) {
		const errorMessage = `
		<p class="error">Something went wrong!</p>
		`;
		chat.insertAdjacentHTML("afterend", errorMessage);
		return;
	}

	const messages = await responseMessages.json();
	if (messages.length == 0) {
		return;
	}

	const responseUserId = await fetch("/api/get-session-id");
	if (!responseUserId.ok) {
		const errorMessage = `
		<p class="error">Something went wrong!</p>
		`;
		chat.insertAdjacentHTML("afterend", errorMessage);
		return;
	}

	const userId = await responseUserId.json();

	messages.forEach((message) => {
		if (message.sender_fk == userId) {
			const messageElement = `
			<div class="chat-message chat-message-outgoing" data-user="${htmlEncode(
				message.sender_fk
			)}">
			<span>${htmlEncode(message.message_body)}</span>
			</div>
			`;
			chatContainer.insertAdjacentHTML("beforeend", messageElement);
			latestMessageId = message.message_id;
			return;
		}
		const messageElement = `
		<div class="chat-message" data-user="${htmlEncode(message.sender_fk)}">
		<span>${htmlEncode(message.message_body)}</span>
		</div>
		`;

		chatContainer.insertAdjacentHTML("beforeend", messageElement);
		latestMessageId = message.message_id;
	});
	scrollToBottom(chatContainer);
};

if (displayChatButton) {
	displayChatButton.onclick = async () => {
		const response = await fetch("/api/get-chat-id", {
			method: "POST",
		});

		if (!response.ok) {
			const errorMessage = `
			<p class="error">Something went wrong!</p>
			`;
			chat.insertAdjacentHTML("afterend", errorMessage);
			return;
		}
		const { chat_id } = await response.json();

		if (chat_id) {
			chat.style.display = "block";
			chatForm.childNodes[3].value = chat_id;
			getLatestMessages(chat_id);
			setInterval(() => getLatestMessages(chat_id), 500);
			return;
		}
		const responseChat = await fetch("/api/create-chat", {
			method: "POST",
		});

		if (!responseChat.ok) {
			const errorMessage = `
			<p class="error">Something went wrong!</p>
			`;
			chat.insertAdjacentHTML("afterend", errorMessage);
			return;
		}
		const jsonResponse = await responseChat.json();
		const chatId = jsonResponse.chat_id;

		if (chatId) {
			chat.style.display = "block";
			chatForm.childNodes[3].value = chatId;
			getLatestMessages(chatId);
			setInterval(() => getLatestMessages(chatId), 500);
			return;
		}
		const errorMessage = `
		<p class="error">Something went wrong!</p>
		`;
		chat.insertAdjacentHTML("afterend", errorMessage);
	};
}

let interval;

if (customerChats) {
	customerChats.forEach((customerChat) => {
		customerChat.onclick = () => {
			latestMessageId = 0;
			let chatId = event.target.dataset.id;
			chatContainer.innerHTML = "";
			chat.style.display = "block";
			chatForm.childNodes[3].value = chatId;
			getLatestMessages(chatId);
			if (interval) {
				clearInterval(interval);
			}
			interval = setInterval(() => getLatestMessages(chatId), 500);
		};
	});
}

chatForm.onsubmit = async () => {
	event.preventDefault();
	const response = await fetch("/api/send-message", {
		method: "POST",
		body: new FormData(event.target),
	});
	chatForm.childNodes[1].value = "";
};
