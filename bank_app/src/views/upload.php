<?php

session_start();
$logged_in = isset($_SESSION['email']) ?? false;
if (!$logged_in) {
    header('location: /');
    exit;
}

if ($_SESSION['is_employee'] == 1) {
    header('location: /logout');
    exit;
}

if (isset($_POST["submit"]))
{

    $dir = __DIR__ . '/../uploads/';
    $file = $dir . basename($_FILES["fileToUpload"]["name"]);
    $upLoadOk = true;
    $imageFileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));
    $errorMessage;
    $mimetype = mime_content_type($_FILES['fileToUpload']['tmp_name']);

    if ($imageFileType == "pdf" and $mimetype == "application/pdf")
    {
        $uploadOk = true;
    }
    else
    {
        $errorMessage = "The file needs to be in .pdf format!";
        $uploadOk = false;
    }

    if (file_exists($file) and $uploadOk == true)
    {
        $errorMessage = "The file already exists on our server!";
        $uploadOk = false;
    }
    // Define MegaBytes
    define('MB', 1048576);

    if ($_FILES["fileToUpload"]["size"] > 5 * MB and $uploadOk == true)
    {
        $errorMessage = "Your file is too large, maximum 5MB is allowed!";
        $uploadOk = false;
    }

    if ($uploadOk == true)
    {
        if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $file))
        {
            echo "File has been uploaded.";
        }
        else
        {
            echo "There was an error uploading your file!";
        }
    }

}

require_once __DIR__ . '/../templates/upload.php';

?>