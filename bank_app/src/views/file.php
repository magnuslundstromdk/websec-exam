<?php

session_start();
$logged_in = isset($_SESSION['email']) ?? false;
if (!$logged_in) {
    header('location: /');
    exit;
}
if ($_SESSION['is_employee'] == 0) {
    header('location: /logout');
    exit;
}

$filepath = __DIR__ . "/../uploads/$file";
if (file_exists($filepath)) {
    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename="' . $file . '"');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($filepath));
    header('Accept-Ranges: bytes');
    @readfile($filepath);
    ob_clean();
    flush();
    readfile($filepath);
    exit;
} else {
    echo "No such file";
}