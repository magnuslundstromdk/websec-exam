<?php

session_start();
$logged_in = isset($_SESSION['email']) ?? false;
if (!$logged_in) {
    header('location: /');
    exit;
}
if ($_SESSION['is_employee'] == 1) {
    header('location: /logout');
    exit;
}

$email = $_SESSION['email'];

require_once __DIR__ . '/../templates/dashboard.php';