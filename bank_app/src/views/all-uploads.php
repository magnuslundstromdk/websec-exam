<?php

session_start();
$logged_in = isset($_SESSION['email']) ?? false;
if (!$logged_in) {
    header('location: /');
    exit;
}
if ($_SESSION['is_employee'] == 0) {
    header('location: /logout');
    exit;
};

$path = __DIR__ . '/../uploads';
$files = scandir($path); //  HTACCESS IS IN THE UPLOAD FOLDER NOT GOOD

$files = array_diff(scandir($path), array('.', '..'));

require_once __DIR__ . '/../templates/all-uploads.php';