<?php
session_start();

if (!isset($_SESSION['user_id']) || !ctype_digit($_SESSION['user_id'])) {
    http_response_code(403);
    header('location: ../');
    exit();
}

require_once __DIR__ . '/../db/db.php';

$latest_message_id = $_POST['latest_message_id'] ?? 0;
$chat_id = $_POST['chat_id'] ?? false;

if ($_SERVER['REQUEST_METHOD'] === 'POST' && $chat_id) {
    try {
        $statement = $db->prepare('SELECT * FROM chats WHERE chat_id = :chat_id LIMIT 1');
        $statement->bindValue('chat_id', $chat_id);
        $statement->execute();
        $chat = $statement->fetch();

        if(!$chat){
            http_response_code(500);
            echo 'Cannot find chat';
            exit();
        }

        if($_SESSION['is_employee'] == 0 && $chat->customer_fk != $_SESSION['user_id']){
            http_response_code(403);
            echo 'That ain\'t yours';
            exit();
        }

        $statement = $db->prepare('SELECT * FROM chat_messages WHERE chat_fk = :chat_id AND message_id > :latest_message_id');
        $statement->bindValue('latest_message_id', $latest_message_id);
        $statement->bindValue('chat_id', $chat_id);
        $statement->execute();
        $messages = $statement->fetchAll();

        header('content-type: application/json');

        echo json_encode($messages);
    } catch (Exception $ex) {
        echo $ex;
        http_response_code(500);
        exit();
    }
}
