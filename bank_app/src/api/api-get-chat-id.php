<?php
session_start();

if(! isset($_SESSION['user_id']) || ! ctype_digit($_SESSION['user_id'])){
    http_response_code(403);
    header('location: ../');
    exit();
}

require_once __DIR__ . '/../db/db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try{
        $statement = $db->prepare('SELECT chat_id FROM chats WHERE customer_fk = :user_id LIMIT 1');
        $statement->bindValue('user_id', $_SESSION['user_id']);
        $statement->execute();
        $chat_id = $statement->fetch();

        header('content-type: application/json');
        echo '{"chat_id":"'.htmlspecialchars($chat_id->chat_id).'"}';
    }catch(Exception $ex){
        http_response_code(500);
        echo $ex;
        exit();
    }
}